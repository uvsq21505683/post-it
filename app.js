var mysql= require('mysql');
var express= require('express')
var esession=require('express-session');
var bodyParser=require('body-parser');
var path=require('path')
const crypto = require('crypto');


var connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'root',
	password : 'root',
	database : 'postit'
});

function hashPassword(pass) {
    let hash = crypto.createHash('sha256');
    hash.update(pass);
    return hash.digest('hex');
  }

  function xss(t){
    return t
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;")
    .replace(/'/g, "&#039;");
  }

var app=express();

app.use(esession({
    secret: 'secret',
    resave: true,
	saveUninitialized: true
}));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json())

app.get('/',function(demande,reponse){
    reponse.sendFile(path.join(__dirname + '/accueil.html'));
})

app.get('/signup',function(demande,reponse){
    if (demande.session.loggedin) return reponse.redirect("/ajout")
    reponse.sendFile(path.join(__dirname+'/signup.html'))
})

app.get('/admin',function(demande,reponse){

        var requete='SELECT admin FROM user WHERE id=?';
        if(!demande.session.username) return reponse.redirect('/')

        connection.query(requete, [demande.session.username], function(error, results, fields) {
            if(results[0].admin==1) return reponse.sendFile(path.join(__dirname+'/admin.html')) 
            else return reponse.end("Page reserve au admin")
        })
})

app.get('/login',function(demande,reponse){
    if (!demande.session.loggedin)
reponse.sendFile(path.join(__dirname+'/login.html'))
    else 
reponse.send('Déja connecté avec '+demande.session.username)
})

app.get('/logout',function(demande,reponse){
    demande.session.loggedin = false;
    demande.session.username = null;
    reponse.redirect('/')
    reponse.end();
})

app.get('/accueil',function(demande,reponse){
    reponse.sendFile(path.join(__dirname + '/home.html'));

})

app.get('/effacer',function(demande,reponse){
    reponse.sendFile(path.join(__dirname + '/effacer.html'));

})



app.post('/signuphtml',function(demande,reponse) {
    var id=xss(demande.body.id);
    var pass=hashPassword(xss(demande.body.pass))
    var repass=demande.body.repass
    var c=0
    var m=0
    var e=0
    var a=0

    //if(pass=repass) return reponse.send('le mot de pass est erroné avec le deuxieme mot de pass')
    if(id && pass && repass){
        var req='INSERT INTO user(id,password,creer,modifier,effacer,admin) VALUES (?,?,?,?,?,?)';
        connection.query(req, [id,pass,c,m,e,a], function(error, results, fields) {
                if(error) return reponse.send("impossible")
                demande.session.loggedin = true;
                demande.session.username = id;
               // reponse.send('Inscription effectuée');
                reponse.redirect('/ajout');			
                reponse.end(); 
        })}
})

app.post('/loginhtml', function(demande, reponse) {
            var username = xss(demande.body.username);
            var password = hashPassword(xss(demande.body.password));
            
            if (username && password) {
connection.query('SELECT * FROM user WHERE id = ? AND password = ?', 
[username, password], function(error, results, fields) {
                     if (results.length > 0) {
                        demande.session.loggedin = true;
                        demande.session.username = username;
                        reponse.redirect('/ajout');
                    } else {
                        reponse.send('Incorrect Username and/or Password!');
                    }			
                    reponse.end();
                });
            }  else {
                reponse.send('Please enter Username and Password!');
                reponse.end();
        }
        });
        

app.get('/ajout', function(request, response) {
           if (request.session.loggedin) {
              //  response.send('Welcome, ' + request.session.username + '!');
                response.sendFile(path.join(__dirname+'/dessin.html'))
            } else {
               // response.send('Enregistrez vous pour voir cette page !');
             response.redirect('/login')
            }
});

app.get('/popup', function(request, response) {
    if (request.session.loggedin) {
       //  response.send('Welcome, ' + request.session.username + '!');
         response.sendFile(path.join(__dirname+'/popup.html'))
     } else {
        // response.send('Enregistrez vous pour voir cette page !');
      response.redirect('/login')
     }
});

app.get('/popup2', function(request, response) {
    if (request.session.loggedin) {
       //  response.send('Welcome, ' + request.session.username + '!');
         response.sendFile(path.join(__dirname+'/popup2.html'))
     } else {
        // response.send('Enregistrez vous pour voir cette page !');
      response.redirect('/login')
     }
});


app.post('/insereNote', function(demande, reponse) {
    console.log("inser")
    var titre=demande.body.titre;
    var text=demande.body.text
    var id=demande.session.username
    var x=demande.body.x
    var y=demande.body.y
    if (titre && text) {
        var requete='INSERT INTO notes(id,titre,text,x,y) VALUES (?,?,?,?,?)';
        connection.query(requete, [id,titre, text,x,y], function(error, results, fields) {
            if (!error) 
           return reponse.end("YES");
            else if(error)
            return  reponse.end("NO");
        })}
    else reponse.end("Saisir titre et message");
})


app.post('/modifNote', function(demande, reponse) {
    var titre=demande.body.titre;
    var text=demande.body.text
    var id;

    var sql1 = "SELECT id FROM notes WHERE titre = ?";
    connection.query(sql1,[titre],function(err,result){
        if (titre && text && result[0].id==demande.session.username) {
            var sql = "UPDATE `notes` SET `text` = ?WHERE `notes`.`titre` = ?";
            connection.query(sql,[text,titre], function (err, result) {
             if (err) throw err;
             console.log(result.affectedRows + " record(s) updated");
             return reponse.end("Modification faite")
            })
        }   

    })
     
})


app.post('/modifNoteCoord', function(demande, reponse) {
    var titre=demande.body.titre;
    var x=demande.body.x
    var y=demande.body.y
console.log("avant la boucle sql coordonées",x,y,titre)
    if (titre && x && y) {
        var sql = "UPDATE `notes` SET `x` = ?, `y` = ? WHERE `notes`.`titre` = ?";

        connection.query(sql,[x,y,titre], function (err, result) {
         if (err) throw err;
         console.log(result.affectedRows + " coordonnée changées ");
         return reponse.end("Coordonnées changées")
        })
    }
})


app.post('/afficherNotes',function(demande,reponse){

var requete="SELECT * FROM notes"
var data;
connection.query(requete, function (err, result) {
    if (err) throw err;
    data='{ "notes":['
   for(var i=0; i<result.length;i++) {
    data+='{'
    data+=' "titre": "' +result[i].titre
    data+='","text": "'+result[i].text
    data+='","x": "' +result[i].x
    data+='","y": "'+result[i].y
    data+='","auteur": "'+result[i].id
    if(i==result.length-1)  data+='"}'
    else data+='"},';
   }
   data+="]}"
   reponse.setHeader('Content-Type', 'application/json')
   return reponse.end(JSON.stringify(data));
})
})

app.get('/liste',function(demande,reponse) {
    if(demande.session.loggedin){
        var req='SELECT * FROM notes WHERE id = ?';
        connection.query(req, [demande.session.username], function(err, result, fields) {
            if (err) throw err;
            data='{ "notes":['
           for(var i=0; i<result.length;i++) {
            data+='{'
            data+=' "titre": "' +result[i].titre
            data+='","text": "'+result[i].text
            data+='","x": "' +result[i].x
            data+='","y": "'+result[i].y
            data+='","auteur": "'+result[i].id
            if(i==result.length-1)  data+='"}'
            else data+='"},';
           }
           data+="]}"
          reponse.setHeader('Content-Type', 'application/json')
          return  reponse.send(data);
        //console.log(data);
        
        })}
    else return reponse.send("Identifiez vous pour avoir la liste des notes")
})

app.post('/afficherNotesUser',function(demande,reponse) {
    
    if(demande.session.loggedin){
        var req2='SELECT effacer FROM user WHERE id = ?';
        connection.query(req2, [demande.session.username], function(err, result, fields) {
            if(result[0].effacer==0){
        var req='SELECT * FROM notes WHERE id = ?';
        connection.query(req, [demande.session.username], function(err, result, fields) {
            if (err) throw err;
            data='{ "notes":['
           for(var i=0; i<result.length;i++) {
            data+='{'
            data+=' "titre": "' +result[i].titre
            data+='","text": "'+result[i].text
            data+='","x": "' +result[i].x
            data+='","y": "'+result[i].y
            data+='","auteur": "'+result[i].id
            if(i==result.length-1)  data+='"}'
            else data+='"},';
           }
           data+="]}"
          reponse.setHeader('Content-Type', 'application/json')
          return  reponse.send(JSON.stringify(data));
        })}
        else{
            var req3='SELECT * FROM notes';
            connection.query(req3, function(err, result, fields){
                if (err) throw err;
                data1='{ "notes":['
               for(var i=0; i<result.length;i++) {
                data1+='{'
                data1+=' "titre": "' +result[i].titre
                data1+='","text": "'+result[i].text
                data1+='","x": "' +result[i].x
                data1+='","y": "'+result[i].y
                data1+='","auteur": "'+result[i].id
                if(i==result.length-1)  data1+='"}'
                else data1+='"},';
               }
               data1+="]}"
               reponse.setHeader('Content-Type', 'application/json')
               return reponse.end(JSON.stringify(data1));
            })

        }
    })}
    else return reponse.send("Identifiez vous pour avoir la liste des notes")
})



app.post('/effacerPost', function(demande, reponse) {
    var titre=demande.body.titre;
    console.log(titre);
    if (titre) {
        var sql = "DELETE FROM `notes` WHERE `notes`.`titre` = ?";
        connection.query(sql,[titre], function (err, result) {
         if (err) throw err;
         return reponse.end("ok")
        })
    }
})

app.post('/afficherDroitUser', function(demande, reponse) {
    var sql = 'SELECT * FROM user';
    connection.query(sql, function (err, result) {
         if (err) throw err;
         data='{ "users":['
         for(var i=0; i<result.length;i++) {
          data+='{'
          data+=' "id": "' +result[i].id
          data+='","creer": "'+result[i].creer
          data+='","modifier": "' +result[i].modifier
          data+='","effacer": "'+result[i].effacer
          data+='","admin": "'+result[i].admin
          if(i==result.length-1)  data+='"}'
          else data+='"},';
         }
         data+="]}"
        reponse.setHeader('Content-Type', 'application/json')
        return  reponse.send(JSON.stringify(data));
        })
})

app.post('/enregistrerDroitUser', function(demande, reponse) {

     console.log(demande.body.id,demande.body.creer,demande.body.modifier,demande.body.effacer,demande.body.admin)
    var a=demande.body.creer
    var b=demande.body.modifier;
    var c=demande.body.effacer;
    var d=demande.body.admin;

    if (1) {
var sql = "UPDATE `user` SET `creer` =?, `modifier` = ?, `effacer` =?, `admin` =? WHERE `user`.`id` = ?";
        connection.query(sql,[a,b,c,d,demande.body.id], function (err, result) {
         if (err) throw err;
         return reponse.end("end")
        })
    }
})


app.listen(3000);

//app.listen(3000, "192.168.0.12");


//http.createServer(function (req, res)).listen(3000, "192.168.0.12");